#! /usr/local/bin/python

"""
PURPOSE
    I wanted to create a something to remind me to move around while sitting at work;
    I ended up creating a general-use countdown timer

USE
    Pass in duration and message; at end of duration, terminal bell will sound and the
    timer will just repeat again, ad infinitum.

FUTURE
    Wrap it into a Tkinter UI?
"""


import datetime
import sys
import time
from optparse import OptionParser

WHITE = '\033[1;37m'
RED = '\033[91m'
GREEN = '\033[92m'
BLUE = '\033[94m'
YELLOW = '\033[1;33m'
BLINK = '\033[5m'
ENDC = '\033[0m'


def countdown_loop(interim, message):
    remaining = interim = int(interim)*60
    while True:
        min_remaining = remaining/60
        if remaining > 60:
            if min_remaining >= 10:
                sys.stdout.write('%sMinutes remaining: %s\r' % (BLUE, min_remaining))
            else:
                sys.stdout.write('%sMinutes remaining: %s\r' % (BLUE, '0'+str(min_remaining)))
            sys.stdout.flush()
            time.sleep(60)
            remaining -= 60
        else:
            if remaining >= 10:
                sys.stdout.write('%s%sSeconds remaining: %s%s\r' % (YELLOW, BLINK, remaining, ENDC))
            else:
                sys.stdout.write('%s%sSeconds remaining: %s%s\a\r' % (YELLOW, BLINK, '0'+str(remaining), ENDC))
            sys.stdout.flush()
            time.sleep(1)
            remaining -= 1
        if remaining == 0:
            alert(message)
            remaining = interim


def alert(message):
    now = datetime.datetime.strftime(datetime.datetime.now(), '%I:%M %p')
    print '%s[%s] - %s' % (RED, now, message)
    print '\a'*7


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-d', '--duration', default='5')
    parser.add_option('-m', '--message', default='Countdown Complete!')
    options, args = parser.parse_args()

    countdown_loop(options.duration, options.message)

